﻿Option Explicit On
Option Strict On
Imports System.IO

Public Class SubnetCalculator

    Private Const WHAT_IS_THE_PROG_CALLED As String = "Subnet Calculator"

    Private networkAddressInput As String
    Private numOfHostsPerSubnet As Integer

    Private valid As Boolean

    Private networkClass As String
    Private subnetSize As Integer
    Private binaryNetworkOctets As String
    Private ipAddresses As Integer
    Private CIDR As Integer
    Private subnetMask As String
    Private binarySubnetOctets As String
    Private decimalAddress As Long
    Private hexOctets As String
    Private numOfSubnets As Integer

    Private ipRangesArray(,) As String
    'Begin Calculating upon use
    Private Sub CalcButton_Click(sender As Object, e As EventArgs) Handles CalcButton.Click

        'Collect Input Boxes
        If NetworkAddressTextBox.Text = String.Empty Then
            MsgBox("Please enter something in the address box")
        Else
            networkAddressInput = NetworkAddressTextBox.Text
        End If

        Try
            If NumberOfHostsTextBox.Text = String.Empty Then
                MsgBox("Please enter something in the host box")
            Else
                numOfHostsPerSubnet = Convert.ToInt32(NumberOfHostsTextBox.Text)
            End If
        Catch ex As Exception
            MsgBox("Please enter only numbers for host size")
        End Try


        'Validate inputs
        valid = ValidateInputs(networkAddressInput, numOfHostsPerSubnet)



        'Calculations & Functions
        If valid = True Then

            subnetSize = FindSubnetSize(numOfHostsPerSubnet)
            networkClass = FindNetworkClass(networkAddressInput)
            ipAddresses = FindNumOfIpAddresses(networkAddressInput)
            decimalAddress = FindDecimalNotationAddress(networkAddressInput)
            binaryNetworkOctets = FindBinaryOctets(networkAddressInput)
            hexOctets = FindHexOctets(networkAddressInput)
            subnetMask = FindSubnetMask(numOfHostsPerSubnet)
            CIDR = FindSubnetCIDR(numOfHostsPerSubnet)
            binarySubnetOctets = FindBinarySubnetMask(numOfHostsPerSubnet)
            numOfSubnets = FindNumOfSubnets(networkAddressInput, numOfHostsPerSubnet)
            ipRangesArray = FindNetIpAddress(networkAddressInput, numOfHostsPerSubnet)

            'Show all data
            Me.ProgressBar.Maximum = FindNumOfSubnets(networkAddressInput, numOfHostsPerSubnet)
            populateFields()
            UpdateListBox()

        ElseIf valid = False Then
            MsgBox("Please enter only positive numbers & no letters or blank boxes e.g: 192.168...")
        End If

    End Sub
    'Function to make sure my input values are correct and usable.
    Private Function ValidateInputs(ByVal networkAddressEntered As String, ByVal hostsEntered As Integer) As Boolean

        Try
            Dim networkArray() As String = Split(networkAddressEntered, ".")
            Dim numberOfIpsInClass As Integer = FindNumOfIpAddresses(networkAddressInput)
            Dim classLetter As String = FindNetworkClass(networkAddressEntered)

            For index As Integer = 0 To 3
                Dim number As Integer = Convert.ToInt32(networkArray(index))
                If number > 255 Or number < 0 Then
                    Return False
                End If
                If index = 1 And classLetter = "A" And number > 0 Then
                    MsgBox("Please enter a valid A,B, or C address")
                    Return False
                ElseIf index = 2 And classLetter = "A" And number > 0 Then
                    MsgBox("Please enter a valid A,B, or C address")
                    Return False
                ElseIf index = 2 And classLetter = "B" And number > 0 Then
                    MsgBox("Please enter a valid A,B, or C address")
                    Return False
                End If
                If classLetter = "C" And hostsEntered > (2 ^ 8 - 2) Then
                    Return False
                ElseIf classLetter = "B" And hostsEntered > (2 ^ 16 - 2) Then
                    Return False
                ElseIf classLetter = "A" And hostsEntered > (2 ^ 24 - 2) Then
                    Return False
                End If
            Next
            If numberOfIpsInClass < hostsEntered Then
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try

        Return True
    End Function
    'Find subnet size that is required.
    Private Function FindSubnetSize(ByVal numOfHostsPerSubnet As Integer) As Integer

        Dim exponent As Integer = 0

        Do Until 2 ^ exponent >= numOfHostsPerSubnet + 2
            exponent += 1
        Loop

        Return Convert.ToInt32(2 ^ exponent)
    End Function
    'Find Class Letter
    Private Function FindNetworkClass(ByVal ipAddress As String) As String

        Dim octets() As String = ipAddress.Split(".".ToCharArray)
        Dim firstOctet As Integer = Convert.ToInt32(octets(0))

        Select Case firstOctet
            Case 0 To 126
                Return "A"
            Case 127
                Return "Local Host"
            Case 128 To 191
                Return "B"
            Case 192 To 223
                Return "C"
            Case 224 To 239
                Return "D"
            Case 240 To 255
                Return "E"
        End Select

        Return Nothing
    End Function
    'Find how many IP's this class has for use.
    Private Function FindNumOfIpAddresses(ByVal ipAddress As String) As Integer

        Dim networkClass As String = FindNetworkClass(ipAddress)

        Select Case networkClass
            Case "A"
                Return Convert.ToInt32(2 ^ 24)
            Case "B"
                Return Convert.ToInt32(2 ^ 16)
            Case "C"
                Return Convert.ToInt32(2 ^ 8)
            Case "D"
                Return 0
        End Select

        Return Nothing
    End Function
    'Find the IP in decimal notation.
    Private Function FindDecimalNotationAddress(ByVal networkAddress As String) As Long

        Dim octets() As String = networkAddress.Split(".".ToCharArray)

        Dim firstOctet As Long = Convert.ToInt64(Convert.ToInt32(octets(0)) * 2 ^ 24)
        Dim secondOctet As Long = Convert.ToInt64(Convert.ToInt32(octets(1)) * 2 ^ 16)
        Dim thirdOctet As Long = Convert.ToInt64(Convert.ToInt32(octets(2)) * 2 ^ 8)
        Dim fourthOctet As Long = Convert.ToInt64(Convert.ToInt32(octets(3)) * 2 ^ 0)

        Return firstOctet + secondOctet + thirdOctet + fourthOctet
    End Function
    'Find the IP in binary.
    Private Function FindBinaryOctets(ByVal networkAddressInput As String) As String

        Dim octets() As String = Split(networkAddressInput, ".")
        Dim octet As Integer
        Dim binaryString As String = String.Empty

        For index As Integer = 0 To 3
            octet = Convert.ToInt32(octets(index))
            For power As Integer = 7 To 0 Step -1
                If octet >= 2 ^ power Then
                    binaryString &= "1"
                    octet -= Convert.ToInt32(2 ^ power)
                Else
                    binaryString &= "0"
                End If
            Next
            If index <> 3 Then
                binaryString &= "."
            End If
        Next

        Return binaryString

    End Function
    'Find the IP in hex.
    Private Function FindHexOctets(ByVal networkAddressInput As String) As String

        Dim ipOctets() As String = Split(networkAddressInput, ".")
        Dim hexString As String = String.Empty
        Dim hexArray(3) As String

        For index As Integer = 0 To 3
            hexArray(index) = Hex(ipOctets(index))
            hexString &= hexArray(index)
            If hexArray(index) = "0" Then
                hexString &= "0"
            End If
            If index <> 3 Then
                hexString &= "."
            End If
        Next

        Return hexString
    End Function
    'Find CIDR notation for given subnet
    Private Function FindSubnetCIDR(ByVal hosts As Integer) As Integer

        Dim actualBlockSize As Integer = FindSubnetSize(hosts)
        Dim exponent As Integer = 0

        Do Until (2 ^ exponent) = actualBlockSize
            exponent += 1
        Loop

        Return (32 - exponent)
    End Function
    'Find the dotted decimal notation for the subnet mask.
    Private Function FindSubnetMask(ByVal hosts As Integer) As String

        Dim CIDRInSubFunction As Integer = FindSubnetCIDR(hosts)
        Dim bitsAfterLastOctet As Integer = 0
        Dim currentOctetsSubnetValue As Integer = 0

        Dim subnetMask As String = String.Empty

        Select Case CIDRInSubFunction
            Case 8 To 15
                bitsAfterLastOctet = 16 - CIDRInSubFunction
                For power As Integer = 7 To bitsAfterLastOctet Step -1
                    currentOctetsSubnetValue += Convert.ToInt32((2 ^ power))
                Next
                subnetMask = (2 ^ 8 - 1).ToString & "." & currentOctetsSubnetValue.ToString & ".0.0"
            Case 16 To 23
                bitsAfterLastOctet = 24 - CIDRInSubFunction
                For power As Integer = 7 To bitsAfterLastOctet Step -1
                    currentOctetsSubnetValue += Convert.ToInt32((2 ^ power))
                Next
                subnetMask = (2 ^ 8 - 1).ToString & "." & (2 ^ 8 - 1).ToString & "." & _
                    currentOctetsSubnetValue.ToString & ".0"
            Case 24 To 31
                bitsAfterLastOctet = 32 - CIDRInSubFunction
                For power As Integer = 7 To bitsAfterLastOctet Step -1
                    currentOctetsSubnetValue += Convert.ToInt32((2 ^ power))
                Next
                subnetMask = (2 ^ 8 - 1).ToString & "." & (2 ^ 8 - 1).ToString & "." & (2 ^ 8 - 1).ToString _
                    & "." & currentOctetsSubnetValue.ToString
        End Select


        Return subnetMask
    End Function
    'Find dotted decimal notations subnet mask displayed in binary notation.
    Private Function FindBinarySubnetMask(ByVal hosts As Integer) As String

        Dim innerSubnetMask As String = FindSubnetMask(hosts)
        Dim binarySubnetMask As String = FindBinaryOctets(innerSubnetMask)

        Return binarySubnetMask
    End Function
    'Find how many subnets this subnetting will now produce.
    Private Function FindNumOfSubnets(ByVal networkAddress As String, ByVal numOfHostsPerSubnet As Integer) As Integer

        Dim numOfIPAdrresses As Integer = FindNumOfIpAddresses(networkAddress)
        Dim sunbetSize As Integer = FindSubnetSize(numOfHostsPerSubnet)

        Return numOfIPAdrresses \ subnetSize
    End Function
    'Find Ip Address Ranges to display in list box.
    Private Function FindNetIpAddress(ByVal networkAddressInput As String, ByVal numOfHosts As Integer) As String(,)

        Dim networkAddrArray() As String = Split(networkAddressInput, ".")
        Dim numOfBitsFromRight As Integer = (32 - FindSubnetCIDR(numOfHosts))
        Dim classLetter As String = FindNetworkClass(networkAddressInput)
        Dim numberToStepBy As Integer = 0

        Dim amountOfSubs As Integer = FindNumOfSubnets(networkAddressInput, numOfHosts)
        Dim networkIps(amountOfSubs - 1, 1) As String

        Select Case classLetter
            Case "A"
                Dim increment As Integer = 0
                If (2 ^ numOfBitsFromRight - 1) >= Convert.ToInt32(2 ^ 16 - 1) Then
                    numOfBitsFromRight -= 16
                    For index As Integer = 0 To Convert.ToInt32(2 ^ 8 - 1) Step Convert.ToInt32(2 ^ numOfBitsFromRight)
                        networkIps(increment, 0) = networkAddrArray(0) & "." & index & ".255.255"
                        networkIps(increment, 1) = networkAddrArray(0) & "." & index + Convert.ToInt32(2 ^ numOfBitsFromRight - 1) & _
                            ".255.255"
                        increment += 1
                    Next
                ElseIf (2 ^ numOfBitsFromRight - 1) >= Convert.ToInt32(2 ^ 8 - 1) Then
                    numOfBitsFromRight -= 8
                    For index2 As Integer = 0 To Convert.ToInt32(2 ^ 8 - 1)
                        For index As Integer = 0 To Convert.ToInt32(2 ^ 8 - 1) Step Convert.ToInt32(2 ^ numOfBitsFromRight)
                            networkIps(increment, 0) = networkAddrArray(0) & "." & index2 & _
                                "." & index & ".255"
                            networkIps(increment, 1) = networkAddrArray(0) & "." & index2 & _
                                "." & index + Convert.ToInt32(2 ^ numOfBitsFromRight - 1) & ".255"
                            increment += 1
                        Next
                    Next
                Else
                    For index3 As Integer = 0 To Convert.ToInt32(2 ^ 8 - 1)
                        For index As Integer = 0 To Convert.ToInt32(2 ^ 8 - 1)
                            For index2 As Integer = 0 To Convert.ToInt32(2 ^ 8 - 1) Step Convert.ToInt32(2 ^ numOfBitsFromRight)
                                networkIps(increment, 0) = networkAddrArray(0) & "." & index3 & _
                                    "." & index & "." & index2
                                networkIps(increment, 1) = networkAddrArray(0) & "." & index3 & _
                                    "." & index & "." & index2 + Convert.ToInt32(2 ^ numOfBitsFromRight - 1)
                                increment += 1
                            Next
                        Next
                    Next
                End If
            Case "B"
                Dim increment As Integer = 0
                If (2 ^ numOfBitsFromRight - 1) >= Convert.ToInt32(2 ^ 8 - 1) Then
                    numOfBitsFromRight -= 8
                    For index As Integer = 0 To Convert.ToInt32(2 ^ 8 - 1) Step Convert.ToInt32(2 ^ numOfBitsFromRight)
                        networkIps(increment, 0) = networkAddrArray(0) & "." & networkAddrArray(1) & _
                            "." & index & ".255"
                        networkIps(increment, 1) = networkAddrArray(0) & "." & networkAddrArray(1) & _
                           "." & index + Convert.ToInt32(2 ^ numOfBitsFromRight - 1) & ".255"
                        increment += 1
                    Next
                Else
                    For index As Integer = 0 To Convert.ToInt32(2 ^ 8 - 1)
                        For index2 As Integer = 0 To Convert.ToInt32(2 ^ 8) Step Convert.ToInt32(2 ^ numOfBitsFromRight)
                            networkIps(increment, 0) = networkAddrArray(0) & "." & networkAddrArray(1) & _
                                "." & index & "." & index2
                            networkIps(increment, 0) = networkAddrArray(0) & "." & networkAddrArray(1) & _
                                "." & index & "." & index2 + Convert.ToInt32(2 ^ numOfBitsFromRight - 1)
                            increment += 1
                        Next
                    Next
                End If
            Case "C"
                Dim increment As Integer = 0
                For index As Integer = Convert.ToInt32(networkAddrArray(3)) To Convert.ToInt32(2 ^ 8 - 1) Step Convert.ToInt32(2 ^ numOfBitsFromRight)
                    networkIps(increment, 0) = networkAddrArray(0) & "." & networkAddrArray(1) & "." & _
                        networkAddrArray(2) & "." & index
                    networkIps(increment, 1) = networkAddrArray(0) & "." & networkAddrArray(1) & "." & _
                        networkAddrArray(2) & "." & index + Convert.ToInt32(2 ^ numOfBitsFromRight - 1)
                    increment += 1
                Next
        End Select

        Return networkIps
    End Function
    'Display all variables collected from functions and calculations, in their corresponding text boxes.
    Private Sub populateFields()
        NetworkClassTextBox.Text = networkClass
        SubnetSizeTextBox.Text = subnetSize.ToString
        NumberIPAddressesTextBox.Text = ipAddresses.ToString
        HexidecimalOctetTextBox.Text = hexOctets
        DecimalOctetTextBox.Text = networkAddressInput
        DecimalAddressTextBox.Text = decimalAddress.ToString
        BinaryOctetTextBox.Text = binaryNetworkOctets
        SubnetMaskTextBox.Text = subnetMask & " /" & CIDR.ToString
        BinaryMaskTextBox.Text = binarySubnetOctets
        NumberSubnetsTextBox.Text = numOfSubnets.ToString
    End Sub
    'Renew values in list box based on array
    Private Sub UpdateListBox()

        CalcButton.Enabled = False

        Dim increment As Integer = 0
        IpRangesListBox.Items.Clear()
        For index As Integer = 0 To ipRangesArray.GetUpperBound(0)
            IpRangesListBox.Items.Add(ipRangesArray(index, 0) & " - " & ipRangesArray(index, 1))
            ProgressBar.Increment(1)
        Next

        CalcButton.Enabled = True
    End Sub
    'Find number of questions in file needing to be loaded into array.
    Private Function HowManyQuestionsInFile(ByVal fileName As String) As Integer

        Dim lineCount As Integer = 0

        FileOpen(1, fileName, OpenMode.Input)

        Do Until EOF(1)

            LineInput(1)
            lineCount += 1
        Loop
        FileClose(1)

        lineCount = Convert.ToInt32((lineCount - 11) / 2)

        Return lineCount - 1
    End Function
    'Load process with built in fail switch. If it returns false it was not able to load.
    Private Function LoadFileIntoProgram(ByVal fileName As String, ByVal lineCount As Integer) As Boolean

        Try
            FileOpen(1, fileName, OpenMode.Input)

            networkClass = LineInput(1)
            subnetSize = Convert.ToInt32(LineInput(1))
            ipAddresses = Convert.ToInt32(LineInput(1))
            hexOctets = LineInput(1)
            networkAddressInput = LineInput(1)
            decimalAddress = Convert.ToInt64(LineInput(1))
            binaryNetworkOctets = LineInput(1)
            subnetMask = LineInput(1)
            CIDR = Convert.ToInt32(LineInput(1))
            binarySubnetOctets = LineInput(1)
            numOfSubnets = Convert.ToInt32(LineInput(1))

            For Index As Integer = 0 To lineCount
                ipRangesArray(Index, 0) = LineInput(1)
                ipRangesArray(Index, 1) = LineInput(1)
            Next
            FileClose(1)

        Catch ex As Exception
            Return False
        End Try

        Return True
    End Function

    'Button Functions and Event Handling
    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        Close()
    End Sub
    Private Sub SaveAsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SaveAsToolStripMenuItem.Click
        SaveFileDialog.ShowDialog()
    End Sub
    Private Sub SaveFileDialog_FileOk(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles SaveFileDialog.FileOk
        Dim userSaveFile As String = SaveFileDialog.FileName
        Dim simpleSaveFileName As String = System.IO.Path.GetFileNameWithoutExtension(userSaveFile)

        Me.Text = WHAT_IS_THE_PROG_CALLED & " - " & simpleSaveFileName

        FileOpen(1, userSaveFile, OpenMode.Output)

        PrintLine(1, networkClass)
        PrintLine(1, subnetSize.ToString)
        PrintLine(1, ipAddresses.ToString)
        PrintLine(1, hexOctets)
        PrintLine(1, networkAddressInput)
        PrintLine(1, decimalAddress.ToString)
        PrintLine(1, binaryNetworkOctets)
        PrintLine(1, subnetMask)
        PrintLine(1, CIDR.ToString)
        PrintLine(1, binarySubnetOctets)
        PrintLine(1, numOfSubnets.ToString)

        For index As Integer = 0 To ipRangesArray.GetUpperBound(0)
            PrintLine(1, ipRangesArray(index, 0))
            PrintLine(1, ipRangesArray(index, 1))
        Next

        FileClose(1)
    End Sub
    Private Sub OpenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OpenToolStripMenuItem.Click
        OpenFileDialog.ShowDialog()
    End Sub
    Private Sub OpenFileDialog_FileOk(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles OpenFileDialog.FileOk

        Dim fileName As String = OpenFileDialog.FileName
        Dim simpleFileName As String
        Dim lineCount As Integer

        simpleFileName = System.IO.Path.GetFileNameWithoutExtension(fileName)
        Me.Text = WHAT_IS_THE_PROG_CALLED & " - " & simpleFileName

        lineCount = HowManyQuestionsInFile(fileName)
        Me.ProgressBar.Maximum = lineCount

        ReDim ipRangesArray(lineCount, 1)

        If LoadFileIntoProgram(fileName, lineCount) = True Then
            populateFields()
            UpdateListBox()
        Else
            MsgBox("Incorrect Format, re-try please!")
        End If

    End Sub
    Private Sub ClearToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ClearToolStripMenuItem.Click
        IpRangesListBox.Items.Clear()
        NetworkClassTextBox.Text = String.Empty
        SubnetSizeTextBox.Text = String.Empty
        NumberIPAddressesTextBox.Text = String.Empty
        HexidecimalOctetTextBox.Text = String.Empty
        DecimalOctetTextBox.Text = String.Empty
        DecimalAddressTextBox.Text = String.Empty
        BinaryOctetTextBox.Text = String.Empty
        SubnetMaskTextBox.Text = String.Empty
        BinaryMaskTextBox.Text = String.Empty
        NumberSubnetsTextBox.Text = String.Empty
        ReDim ipRangesArray(0, 1)

    End Sub
    Private Sub CalculateToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CalculateToolStripMenuItem.Click

        'Collect Input Boxes
        If NetworkAddressTextBox.Text = String.Empty Then
            MsgBox("Please enter something in the address box")
        Else
            networkAddressInput = NetworkAddressTextBox.Text
        End If

        Try
            If NumberOfHostsTextBox.Text = String.Empty Then
                MsgBox("Please enter something in the host box")
            Else
                numOfHostsPerSubnet = Convert.ToInt32(NumberOfHostsTextBox.Text)
            End If
        Catch ex As Exception
            MsgBox("Please enter only numbers for host size")
        End Try


        'Validate inputs
        valid = ValidateInputs(networkAddressInput, numOfHostsPerSubnet)



        'Calculations & Functions
        If valid = True Then

            subnetSize = FindSubnetSize(numOfHostsPerSubnet)
            networkClass = FindNetworkClass(networkAddressInput)
            ipAddresses = FindNumOfIpAddresses(networkAddressInput)
            decimalAddress = FindDecimalNotationAddress(networkAddressInput)
            binaryNetworkOctets = FindBinaryOctets(networkAddressInput)
            hexOctets = FindHexOctets(networkAddressInput)
            subnetMask = FindSubnetMask(numOfHostsPerSubnet)
            CIDR = FindSubnetCIDR(numOfHostsPerSubnet)
            binarySubnetOctets = FindBinarySubnetMask(numOfHostsPerSubnet)
            numOfSubnets = FindNumOfSubnets(networkAddressInput, numOfHostsPerSubnet)
            ipRangesArray = FindNetIpAddress(networkAddressInput, numOfHostsPerSubnet)

            'Show all data
            Me.ProgressBar.Maximum = FindNumOfSubnets(networkAddressInput, numOfHostsPerSubnet)
            populateFields()
            UpdateListBox()

        ElseIf valid = False Then
            MsgBox("Please enter only positive numbers & no letters or blank boxes e.g: 192.168...")
        End If

    End Sub
End Class