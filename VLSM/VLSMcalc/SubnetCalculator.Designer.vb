﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SubnetCalculator
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaveAsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CalculateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClearToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MainVertSplitContainer = New System.Windows.Forms.SplitContainer()
        Me.LeftHorizSplitContainer = New System.Windows.Forms.SplitContainer()
        Me.NetworkGroupBox = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SubnetSizeTextBox = New System.Windows.Forms.TextBox()
        Me.NumberOfHostsTextBox = New System.Windows.Forms.TextBox()
        Me.NetworkAddressTextBox = New System.Windows.Forms.TextBox()
        Me.NetworkAddressLabel = New System.Windows.Forms.Label()
        Me.IPListGroupBox = New System.Windows.Forms.GroupBox()
        Me.ProgressBar = New System.Windows.Forms.ProgressBar()
        Me.IpRangesListBox = New System.Windows.Forms.ListBox()
        Me.RightHorizSplitContainer = New System.Windows.Forms.SplitContainer()
        Me.AddressGroupBox = New System.Windows.Forms.GroupBox()
        Me.HexidecimalOctetLabel = New System.Windows.Forms.Label()
        Me.BinaryOctetLabel = New System.Windows.Forms.Label()
        Me.DecimalAddressLabel = New System.Windows.Forms.Label()
        Me.DecimalOctetLabel = New System.Windows.Forms.Label()
        Me.HexidecimalOctetTextBox = New System.Windows.Forms.TextBox()
        Me.BinaryOctetTextBox = New System.Windows.Forms.TextBox()
        Me.DecimalAddressTextBox = New System.Windows.Forms.TextBox()
        Me.DecimalOctetTextBox = New System.Windows.Forms.TextBox()
        Me.NetmaskGroupBox = New System.Windows.Forms.GroupBox()
        Me.CalcButton = New System.Windows.Forms.Button()
        Me.NumberSubnetsLabel = New System.Windows.Forms.Label()
        Me.BinaryMaskLabel = New System.Windows.Forms.Label()
        Me.SubnetMaskLabel = New System.Windows.Forms.Label()
        Me.NumberIPAddressesLabel = New System.Windows.Forms.Label()
        Me.NetworkClassLabel = New System.Windows.Forms.Label()
        Me.NumberSubnetsTextBox = New System.Windows.Forms.TextBox()
        Me.BinaryMaskTextBox = New System.Windows.Forms.TextBox()
        Me.SubnetMaskTextBox = New System.Windows.Forms.TextBox()
        Me.NumberIPAddressesTextBox = New System.Windows.Forms.TextBox()
        Me.NetworkClassTextBox = New System.Windows.Forms.TextBox()
        Me.SaveFileDialog = New System.Windows.Forms.SaveFileDialog()
        Me.OpenFileDialog = New System.Windows.Forms.OpenFileDialog()
        Me.MenuStrip1.SuspendLayout()
        Me.MainVertSplitContainer.Panel1.SuspendLayout()
        Me.MainVertSplitContainer.Panel2.SuspendLayout()
        Me.MainVertSplitContainer.SuspendLayout()
        Me.LeftHorizSplitContainer.Panel1.SuspendLayout()
        Me.LeftHorizSplitContainer.Panel2.SuspendLayout()
        Me.LeftHorizSplitContainer.SuspendLayout()
        Me.NetworkGroupBox.SuspendLayout()
        Me.IPListGroupBox.SuspendLayout()
        Me.RightHorizSplitContainer.Panel1.SuspendLayout()
        Me.RightHorizSplitContainer.Panel2.SuspendLayout()
        Me.RightHorizSplitContainer.SuspendLayout()
        Me.AddressGroupBox.SuspendLayout()
        Me.NetmaskGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.SystemColors.HotTrack
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.ToolsToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(7, 3, 0, 3)
        Me.MenuStrip1.Size = New System.Drawing.Size(832, 30)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MainMenuStrip"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OpenToolStripMenuItem, Me.SaveAsToolStripMenuItem, Me.ToolStripSeparator1, Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 11.0!)
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(44, 24)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'OpenToolStripMenuItem
        '
        Me.OpenToolStripMenuItem.Name = "OpenToolStripMenuItem"
        Me.OpenToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me.OpenToolStripMenuItem.Size = New System.Drawing.Size(188, 24)
        Me.OpenToolStripMenuItem.Text = "Open"
        '
        'SaveAsToolStripMenuItem
        '
        Me.SaveAsToolStripMenuItem.Name = "SaveAsToolStripMenuItem"
        Me.SaveAsToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.SaveAsToolStripMenuItem.Size = New System.Drawing.Size(188, 24)
        Me.SaveAsToolStripMenuItem.Text = "Save As..."
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(149, 6)
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.F4), System.Windows.Forms.Keys)
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(188, 24)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'ToolsToolStripMenuItem
        '
        Me.ToolsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CalculateToolStripMenuItem, Me.ClearToolStripMenuItem})
        Me.ToolsToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 11.0!)
        Me.ToolsToolStripMenuItem.Name = "ToolsToolStripMenuItem"
        Me.ToolsToolStripMenuItem.Size = New System.Drawing.Size(57, 24)
        Me.ToolsToolStripMenuItem.Text = "Tools"
        '
        'CalculateToolStripMenuItem
        '
        Me.CalculateToolStripMenuItem.Name = "CalculateToolStripMenuItem"
        Me.CalculateToolStripMenuItem.Size = New System.Drawing.Size(152, 24)
        Me.CalculateToolStripMenuItem.Text = "Calculate"
        '
        'ClearToolStripMenuItem
        '
        Me.ClearToolStripMenuItem.Name = "ClearToolStripMenuItem"
        Me.ClearToolStripMenuItem.Size = New System.Drawing.Size(152, 24)
        Me.ClearToolStripMenuItem.Text = "Clear"
        '
        'MainVertSplitContainer
        '
        Me.MainVertSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MainVertSplitContainer.Location = New System.Drawing.Point(0, 30)
        Me.MainVertSplitContainer.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MainVertSplitContainer.Name = "MainVertSplitContainer"
        '
        'MainVertSplitContainer.Panel1
        '
        Me.MainVertSplitContainer.Panel1.Controls.Add(Me.LeftHorizSplitContainer)
        '
        'MainVertSplitContainer.Panel2
        '
        Me.MainVertSplitContainer.Panel2.Controls.Add(Me.RightHorizSplitContainer)
        Me.MainVertSplitContainer.Size = New System.Drawing.Size(832, 478)
        Me.MainVertSplitContainer.SplitterDistance = 394
        Me.MainVertSplitContainer.SplitterWidth = 5
        Me.MainVertSplitContainer.TabIndex = 1
        '
        'LeftHorizSplitContainer
        '
        Me.LeftHorizSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LeftHorizSplitContainer.Location = New System.Drawing.Point(0, 0)
        Me.LeftHorizSplitContainer.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LeftHorizSplitContainer.Name = "LeftHorizSplitContainer"
        Me.LeftHorizSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'LeftHorizSplitContainer.Panel1
        '
        Me.LeftHorizSplitContainer.Panel1.Controls.Add(Me.NetworkGroupBox)
        '
        'LeftHorizSplitContainer.Panel2
        '
        Me.LeftHorizSplitContainer.Panel2.Controls.Add(Me.IPListGroupBox)
        Me.LeftHorizSplitContainer.Size = New System.Drawing.Size(394, 478)
        Me.LeftHorizSplitContainer.SplitterDistance = 151
        Me.LeftHorizSplitContainer.SplitterWidth = 5
        Me.LeftHorizSplitContainer.TabIndex = 0
        '
        'NetworkGroupBox
        '
        Me.NetworkGroupBox.Controls.Add(Me.Label2)
        Me.NetworkGroupBox.Controls.Add(Me.Label1)
        Me.NetworkGroupBox.Controls.Add(Me.SubnetSizeTextBox)
        Me.NetworkGroupBox.Controls.Add(Me.NumberOfHostsTextBox)
        Me.NetworkGroupBox.Controls.Add(Me.NetworkAddressTextBox)
        Me.NetworkGroupBox.Controls.Add(Me.NetworkAddressLabel)
        Me.NetworkGroupBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.NetworkGroupBox.Location = New System.Drawing.Point(0, 0)
        Me.NetworkGroupBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.NetworkGroupBox.Name = "NetworkGroupBox"
        Me.NetworkGroupBox.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.NetworkGroupBox.Size = New System.Drawing.Size(394, 151)
        Me.NetworkGroupBox.TabIndex = 0
        Me.NetworkGroupBox.TabStop = False
        Me.NetworkGroupBox.Text = "Network Information"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Franklin Gothic Medium", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(23, 104)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(148, 18)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Calculated Subnet Size:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Franklin Gothic Medium", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(23, 69)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(137, 18)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "# of Hosts per Subnet"
        '
        'SubnetSizeTextBox
        '
        Me.SubnetSizeTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SubnetSizeTextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.SubnetSizeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SubnetSizeTextBox.Location = New System.Drawing.Point(222, 104)
        Me.SubnetSizeTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.SubnetSizeTextBox.Name = "SubnetSizeTextBox"
        Me.SubnetSizeTextBox.ReadOnly = True
        Me.SubnetSizeTextBox.Size = New System.Drawing.Size(116, 22)
        Me.SubnetSizeTextBox.TabIndex = 5
        '
        'NumberOfHostsTextBox
        '
        Me.NumberOfHostsTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.NumberOfHostsTextBox.Location = New System.Drawing.Point(222, 69)
        Me.NumberOfHostsTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.NumberOfHostsTextBox.Name = "NumberOfHostsTextBox"
        Me.NumberOfHostsTextBox.Size = New System.Drawing.Size(116, 22)
        Me.NumberOfHostsTextBox.TabIndex = 4
        '
        'NetworkAddressTextBox
        '
        Me.NetworkAddressTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.NetworkAddressTextBox.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.NetworkAddressTextBox.Location = New System.Drawing.Point(222, 34)
        Me.NetworkAddressTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.NetworkAddressTextBox.Name = "NetworkAddressTextBox"
        Me.NetworkAddressTextBox.Size = New System.Drawing.Size(156, 22)
        Me.NetworkAddressTextBox.TabIndex = 1
        '
        'NetworkAddressLabel
        '
        Me.NetworkAddressLabel.AutoSize = True
        Me.NetworkAddressLabel.Font = New System.Drawing.Font("Franklin Gothic Medium", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NetworkAddressLabel.Location = New System.Drawing.Point(23, 36)
        Me.NetworkAddressLabel.Name = "NetworkAddressLabel"
        Me.NetworkAddressLabel.Size = New System.Drawing.Size(111, 18)
        Me.NetworkAddressLabel.TabIndex = 0
        Me.NetworkAddressLabel.Text = "Network Address:"
        '
        'IPListGroupBox
        '
        Me.IPListGroupBox.Controls.Add(Me.ProgressBar)
        Me.IPListGroupBox.Controls.Add(Me.IpRangesListBox)
        Me.IPListGroupBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.IPListGroupBox.Location = New System.Drawing.Point(0, 0)
        Me.IPListGroupBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.IPListGroupBox.Name = "IPListGroupBox"
        Me.IPListGroupBox.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.IPListGroupBox.Size = New System.Drawing.Size(394, 322)
        Me.IPListGroupBox.TabIndex = 0
        Me.IPListGroupBox.TabStop = False
        Me.IPListGroupBox.Text = "List of IP Ranges"
        '
        'ProgressBar
        '
        Me.ProgressBar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ProgressBar.ForeColor = System.Drawing.Color.DarkGreen
        Me.ProgressBar.Location = New System.Drawing.Point(7, 21)
        Me.ProgressBar.Maximum = 15
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Size = New System.Drawing.Size(382, 23)
        Me.ProgressBar.TabIndex = 1
        '
        'IpRangesListBox
        '
        Me.IpRangesListBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.IpRangesListBox.FormattingEnabled = True
        Me.IpRangesListBox.ItemHeight = 17
        Me.IpRangesListBox.Location = New System.Drawing.Point(7, 40)
        Me.IpRangesListBox.Name = "IpRangesListBox"
        Me.IpRangesListBox.Size = New System.Drawing.Size(381, 276)
        Me.IpRangesListBox.TabIndex = 0
        '
        'RightHorizSplitContainer
        '
        Me.RightHorizSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RightHorizSplitContainer.Location = New System.Drawing.Point(0, 0)
        Me.RightHorizSplitContainer.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.RightHorizSplitContainer.Name = "RightHorizSplitContainer"
        Me.RightHorizSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'RightHorizSplitContainer.Panel1
        '
        Me.RightHorizSplitContainer.Panel1.Controls.Add(Me.AddressGroupBox)
        '
        'RightHorizSplitContainer.Panel2
        '
        Me.RightHorizSplitContainer.Panel2.Controls.Add(Me.NetmaskGroupBox)
        Me.RightHorizSplitContainer.Size = New System.Drawing.Size(433, 478)
        Me.RightHorizSplitContainer.SplitterDistance = 200
        Me.RightHorizSplitContainer.SplitterWidth = 5
        Me.RightHorizSplitContainer.TabIndex = 0
        '
        'AddressGroupBox
        '
        Me.AddressGroupBox.Controls.Add(Me.HexidecimalOctetLabel)
        Me.AddressGroupBox.Controls.Add(Me.BinaryOctetLabel)
        Me.AddressGroupBox.Controls.Add(Me.DecimalAddressLabel)
        Me.AddressGroupBox.Controls.Add(Me.DecimalOctetLabel)
        Me.AddressGroupBox.Controls.Add(Me.HexidecimalOctetTextBox)
        Me.AddressGroupBox.Controls.Add(Me.BinaryOctetTextBox)
        Me.AddressGroupBox.Controls.Add(Me.DecimalAddressTextBox)
        Me.AddressGroupBox.Controls.Add(Me.DecimalOctetTextBox)
        Me.AddressGroupBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.AddressGroupBox.Location = New System.Drawing.Point(0, 0)
        Me.AddressGroupBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.AddressGroupBox.Name = "AddressGroupBox"
        Me.AddressGroupBox.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.AddressGroupBox.Size = New System.Drawing.Size(433, 200)
        Me.AddressGroupBox.TabIndex = 0
        Me.AddressGroupBox.TabStop = False
        Me.AddressGroupBox.Text = "Address Information"
        '
        'HexidecimalOctetLabel
        '
        Me.HexidecimalOctetLabel.AutoSize = True
        Me.HexidecimalOctetLabel.Location = New System.Drawing.Point(21, 136)
        Me.HexidecimalOctetLabel.Name = "HexidecimalOctetLabel"
        Me.HexidecimalOctetLabel.Size = New System.Drawing.Size(116, 17)
        Me.HexidecimalOctetLabel.TabIndex = 15
        Me.HexidecimalOctetLabel.Text = "Hexidecimal Octets:"
        '
        'BinaryOctetLabel
        '
        Me.BinaryOctetLabel.AutoSize = True
        Me.BinaryOctetLabel.Location = New System.Drawing.Point(21, 102)
        Me.BinaryOctetLabel.Name = "BinaryOctetLabel"
        Me.BinaryOctetLabel.Size = New System.Drawing.Size(83, 17)
        Me.BinaryOctetLabel.TabIndex = 14
        Me.BinaryOctetLabel.Text = "Binary Octets:"
        '
        'DecimalAddressLabel
        '
        Me.DecimalAddressLabel.AutoSize = True
        Me.DecimalAddressLabel.Location = New System.Drawing.Point(21, 67)
        Me.DecimalAddressLabel.Name = "DecimalAddressLabel"
        Me.DecimalAddressLabel.Size = New System.Drawing.Size(105, 17)
        Me.DecimalAddressLabel.TabIndex = 13
        Me.DecimalAddressLabel.Text = "Decimal Address:"
        '
        'DecimalOctetLabel
        '
        Me.DecimalOctetLabel.AutoSize = True
        Me.DecimalOctetLabel.Location = New System.Drawing.Point(21, 34)
        Me.DecimalOctetLabel.Name = "DecimalOctetLabel"
        Me.DecimalOctetLabel.Size = New System.Drawing.Size(95, 17)
        Me.DecimalOctetLabel.TabIndex = 12
        Me.DecimalOctetLabel.Text = "Decimal Octets:"
        '
        'HexidecimalOctetTextBox
        '
        Me.HexidecimalOctetTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.HexidecimalOctetTextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.HexidecimalOctetTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.HexidecimalOctetTextBox.Location = New System.Drawing.Point(142, 132)
        Me.HexidecimalOctetTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.HexidecimalOctetTextBox.Name = "HexidecimalOctetTextBox"
        Me.HexidecimalOctetTextBox.ReadOnly = True
        Me.HexidecimalOctetTextBox.Size = New System.Drawing.Size(285, 22)
        Me.HexidecimalOctetTextBox.TabIndex = 11
        '
        'BinaryOctetTextBox
        '
        Me.BinaryOctetTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BinaryOctetTextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.BinaryOctetTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BinaryOctetTextBox.Location = New System.Drawing.Point(142, 101)
        Me.BinaryOctetTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.BinaryOctetTextBox.Name = "BinaryOctetTextBox"
        Me.BinaryOctetTextBox.ReadOnly = True
        Me.BinaryOctetTextBox.Size = New System.Drawing.Size(285, 22)
        Me.BinaryOctetTextBox.TabIndex = 10
        '
        'DecimalAddressTextBox
        '
        Me.DecimalAddressTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DecimalAddressTextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.DecimalAddressTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DecimalAddressTextBox.Location = New System.Drawing.Point(142, 66)
        Me.DecimalAddressTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.DecimalAddressTextBox.Name = "DecimalAddressTextBox"
        Me.DecimalAddressTextBox.ReadOnly = True
        Me.DecimalAddressTextBox.Size = New System.Drawing.Size(285, 22)
        Me.DecimalAddressTextBox.TabIndex = 9
        '
        'DecimalOctetTextBox
        '
        Me.DecimalOctetTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DecimalOctetTextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.DecimalOctetTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DecimalOctetTextBox.Location = New System.Drawing.Point(142, 31)
        Me.DecimalOctetTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.DecimalOctetTextBox.Name = "DecimalOctetTextBox"
        Me.DecimalOctetTextBox.ReadOnly = True
        Me.DecimalOctetTextBox.Size = New System.Drawing.Size(285, 22)
        Me.DecimalOctetTextBox.TabIndex = 8
        '
        'NetmaskGroupBox
        '
        Me.NetmaskGroupBox.Controls.Add(Me.CalcButton)
        Me.NetmaskGroupBox.Controls.Add(Me.NumberSubnetsLabel)
        Me.NetmaskGroupBox.Controls.Add(Me.BinaryMaskLabel)
        Me.NetmaskGroupBox.Controls.Add(Me.SubnetMaskLabel)
        Me.NetmaskGroupBox.Controls.Add(Me.NumberIPAddressesLabel)
        Me.NetmaskGroupBox.Controls.Add(Me.NetworkClassLabel)
        Me.NetmaskGroupBox.Controls.Add(Me.NumberSubnetsTextBox)
        Me.NetmaskGroupBox.Controls.Add(Me.BinaryMaskTextBox)
        Me.NetmaskGroupBox.Controls.Add(Me.SubnetMaskTextBox)
        Me.NetmaskGroupBox.Controls.Add(Me.NumberIPAddressesTextBox)
        Me.NetmaskGroupBox.Controls.Add(Me.NetworkClassTextBox)
        Me.NetmaskGroupBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.NetmaskGroupBox.Location = New System.Drawing.Point(0, 0)
        Me.NetmaskGroupBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.NetmaskGroupBox.Name = "NetmaskGroupBox"
        Me.NetmaskGroupBox.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.NetmaskGroupBox.Size = New System.Drawing.Size(433, 273)
        Me.NetmaskGroupBox.TabIndex = 0
        Me.NetmaskGroupBox.TabStop = False
        Me.NetmaskGroupBox.Text = "Netmask Information"
        '
        'CalcButton
        '
        Me.CalcButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CalcButton.Location = New System.Drawing.Point(233, 240)
        Me.CalcButton.Name = "CalcButton"
        Me.CalcButton.Size = New System.Drawing.Size(137, 26)
        Me.CalcButton.TabIndex = 26
        Me.CalcButton.Text = "Calculate"
        Me.CalcButton.UseVisualStyleBackColor = True
        '
        'NumberSubnetsLabel
        '
        Me.NumberSubnetsLabel.AutoSize = True
        Me.NumberSubnetsLabel.Location = New System.Drawing.Point(21, 206)
        Me.NumberSubnetsLabel.Name = "NumberSubnetsLabel"
        Me.NumberSubnetsLabel.Size = New System.Drawing.Size(81, 17)
        Me.NumberSubnetsLabel.TabIndex = 25
        Me.NumberSubnetsLabel.Text = "# of Subnets:"
        '
        'BinaryMaskLabel
        '
        Me.BinaryMaskLabel.AutoSize = True
        Me.BinaryMaskLabel.Location = New System.Drawing.Point(21, 164)
        Me.BinaryMaskLabel.Name = "BinaryMaskLabel"
        Me.BinaryMaskLabel.Size = New System.Drawing.Size(78, 17)
        Me.BinaryMaskLabel.TabIndex = 24
        Me.BinaryMaskLabel.Text = "Binary Mask:"
        '
        'SubnetMaskLabel
        '
        Me.SubnetMaskLabel.AutoSize = True
        Me.SubnetMaskLabel.Location = New System.Drawing.Point(21, 122)
        Me.SubnetMaskLabel.Name = "SubnetMaskLabel"
        Me.SubnetMaskLabel.Size = New System.Drawing.Size(83, 17)
        Me.SubnetMaskLabel.TabIndex = 23
        Me.SubnetMaskLabel.Text = "Subnet Mask:"
        '
        'NumberIPAddressesLabel
        '
        Me.NumberIPAddressesLabel.AutoSize = True
        Me.NumberIPAddressesLabel.Location = New System.Drawing.Point(21, 81)
        Me.NumberIPAddressesLabel.Name = "NumberIPAddressesLabel"
        Me.NumberIPAddressesLabel.Size = New System.Drawing.Size(108, 17)
        Me.NumberIPAddressesLabel.TabIndex = 22
        Me.NumberIPAddressesLabel.Text = "# of IP Addresses:"
        '
        'NetworkClassLabel
        '
        Me.NetworkClassLabel.AutoSize = True
        Me.NetworkClassLabel.Location = New System.Drawing.Point(21, 42)
        Me.NetworkClassLabel.Name = "NetworkClassLabel"
        Me.NetworkClassLabel.Size = New System.Drawing.Size(90, 17)
        Me.NetworkClassLabel.TabIndex = 21
        Me.NetworkClassLabel.Text = "Network Class:"
        '
        'NumberSubnetsTextBox
        '
        Me.NumberSubnetsTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.NumberSubnetsTextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.NumberSubnetsTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NumberSubnetsTextBox.Location = New System.Drawing.Point(142, 201)
        Me.NumberSubnetsTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.NumberSubnetsTextBox.Name = "NumberSubnetsTextBox"
        Me.NumberSubnetsTextBox.ReadOnly = True
        Me.NumberSubnetsTextBox.Size = New System.Drawing.Size(93, 22)
        Me.NumberSubnetsTextBox.TabIndex = 20
        '
        'BinaryMaskTextBox
        '
        Me.BinaryMaskTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BinaryMaskTextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.BinaryMaskTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BinaryMaskTextBox.Location = New System.Drawing.Point(142, 159)
        Me.BinaryMaskTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.BinaryMaskTextBox.Name = "BinaryMaskTextBox"
        Me.BinaryMaskTextBox.ReadOnly = True
        Me.BinaryMaskTextBox.Size = New System.Drawing.Size(285, 22)
        Me.BinaryMaskTextBox.TabIndex = 19
        '
        'SubnetMaskTextBox
        '
        Me.SubnetMaskTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SubnetMaskTextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.SubnetMaskTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SubnetMaskTextBox.Location = New System.Drawing.Point(142, 117)
        Me.SubnetMaskTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.SubnetMaskTextBox.Name = "SubnetMaskTextBox"
        Me.SubnetMaskTextBox.ReadOnly = True
        Me.SubnetMaskTextBox.Size = New System.Drawing.Size(285, 22)
        Me.SubnetMaskTextBox.TabIndex = 18
        '
        'NumberIPAddressesTextBox
        '
        Me.NumberIPAddressesTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.NumberIPAddressesTextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.NumberIPAddressesTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NumberIPAddressesTextBox.Location = New System.Drawing.Point(142, 76)
        Me.NumberIPAddressesTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.NumberIPAddressesTextBox.Name = "NumberIPAddressesTextBox"
        Me.NumberIPAddressesTextBox.ReadOnly = True
        Me.NumberIPAddressesTextBox.Size = New System.Drawing.Size(93, 22)
        Me.NumberIPAddressesTextBox.TabIndex = 17
        '
        'NetworkClassTextBox
        '
        Me.NetworkClassTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.NetworkClassTextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.NetworkClassTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NetworkClassTextBox.Location = New System.Drawing.Point(142, 37)
        Me.NetworkClassTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.NetworkClassTextBox.Name = "NetworkClassTextBox"
        Me.NetworkClassTextBox.ReadOnly = True
        Me.NetworkClassTextBox.Size = New System.Drawing.Size(44, 22)
        Me.NetworkClassTextBox.TabIndex = 16
        '
        'SaveFileDialog
        '
        Me.SaveFileDialog.Filter = "(*.txt) | *.txt"
        '
        'OpenFileDialog
        '
        Me.OpenFileDialog.FileName = "OpenFileDialog1"
        '
        'SubnetCalculator
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(832, 508)
        Me.Controls.Add(Me.MainVertSplitContainer)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Font = New System.Drawing.Font("Franklin Gothic Medium", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MinimumSize = New System.Drawing.Size(848, 546)
        Me.Name = "SubnetCalculator"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Subnet Calculator"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.MainVertSplitContainer.Panel1.ResumeLayout(False)
        Me.MainVertSplitContainer.Panel2.ResumeLayout(False)
        Me.MainVertSplitContainer.ResumeLayout(False)
        Me.LeftHorizSplitContainer.Panel1.ResumeLayout(False)
        Me.LeftHorizSplitContainer.Panel2.ResumeLayout(False)
        Me.LeftHorizSplitContainer.ResumeLayout(False)
        Me.NetworkGroupBox.ResumeLayout(False)
        Me.NetworkGroupBox.PerformLayout()
        Me.IPListGroupBox.ResumeLayout(False)
        Me.RightHorizSplitContainer.Panel1.ResumeLayout(False)
        Me.RightHorizSplitContainer.Panel2.ResumeLayout(False)
        Me.RightHorizSplitContainer.ResumeLayout(False)
        Me.AddressGroupBox.ResumeLayout(False)
        Me.AddressGroupBox.PerformLayout()
        Me.NetmaskGroupBox.ResumeLayout(False)
        Me.NetmaskGroupBox.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaveAsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CalculateToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClearToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MainVertSplitContainer As System.Windows.Forms.SplitContainer
    Friend WithEvents LeftHorizSplitContainer As System.Windows.Forms.SplitContainer
    Friend WithEvents RightHorizSplitContainer As System.Windows.Forms.SplitContainer
    Friend WithEvents NetworkGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents IPListGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents AddressGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents NetmaskGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents SubnetSizeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NumberOfHostsTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NetworkAddressTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NetworkAddressLabel As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DecimalOctetTextBox As System.Windows.Forms.TextBox
    Friend WithEvents HexidecimalOctetLabel As System.Windows.Forms.Label
    Friend WithEvents BinaryOctetLabel As System.Windows.Forms.Label
    Friend WithEvents DecimalAddressLabel As System.Windows.Forms.Label
    Friend WithEvents DecimalOctetLabel As System.Windows.Forms.Label
    Friend WithEvents HexidecimalOctetTextBox As System.Windows.Forms.TextBox
    Friend WithEvents BinaryOctetTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DecimalAddressTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NumberSubnetsTextBox As System.Windows.Forms.TextBox
    Friend WithEvents BinaryMaskTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SubnetMaskTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NumberIPAddressesTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NetworkClassTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NumberIPAddressesLabel As System.Windows.Forms.Label
    Friend WithEvents NetworkClassLabel As System.Windows.Forms.Label
    Friend WithEvents SubnetMaskLabel As System.Windows.Forms.Label
    Friend WithEvents NumberSubnetsLabel As System.Windows.Forms.Label
    Friend WithEvents BinaryMaskLabel As System.Windows.Forms.Label
    Friend WithEvents CalcButton As System.Windows.Forms.Button
    Friend WithEvents ProgressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents IpRangesListBox As System.Windows.Forms.ListBox
    Friend WithEvents SaveFileDialog As System.Windows.Forms.SaveFileDialog
    Friend WithEvents OpenFileDialog As System.Windows.Forms.OpenFileDialog

End Class
